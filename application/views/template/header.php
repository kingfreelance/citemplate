    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title><?= $page_title;?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    
    <link rel='stylesheet' type='text/css' media='screen' href='<?= base_url();?>assets/css/bootstrap.min.css'>
    <script src='<?= base_url();?>assets/js/jquery.min.js'></script>
    <script src='<?= base_url();?>assets/js/bootstrap.min.js'></script>
    <script src='<?= base_url();?>assets/js/bootbox.min.js'></script>
    
    
    <link rel='stylesheet' type='text/css' media='screen' href='<?= base_url();?>assets/css/custom.min.css'>
    <script src='<?= base_url();?>assets/js/custom.min.js>'></script>